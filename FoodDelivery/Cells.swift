//
//  Cells.swift
//  FoodDelivery
//
//  Created by  Amber  on 11/30/21.
//

import UIKit
import SwiftUI

class BannerCell: UICollectionViewCell {
    
    let bannerImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "banner")
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(bannerImage)
        
        bannerImage.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        bannerImage.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        bannerImage.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        bannerImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CategoryCell: UICollectionViewCell {
    
    let logoImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "banana")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Food"
        label.textColor = .black
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(logoImage)
        self.addSubview(label)
        
        logoImage.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 5).isActive = true
        logoImage.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -5).isActive = true
        logoImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
        logoImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -30).isActive = true
        
        label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

enum Category {
    case Food,Drinks,Desserts,Fruits,Soups,Vegetables,Salads,Pizza
    
    var image: UIImage {
        switch self{
        case .Drinks:
            return UIImage(named: "soda") ?? UIImage()
        case .Salads:
            return UIImage(named: "salad") ?? UIImage()
        case .Food:
            return UIImage(named: "burger") ?? UIImage()
        case .Vegetables:
            return UIImage(named: "broccoli") ?? UIImage()
        case .Pizza:
            return UIImage(named: "pizza") ?? UIImage()
        case .Soups:
            return UIImage(named: "soup") ?? UIImage()
        case .Desserts:
            return UIImage(named: "dessert") ?? UIImage()
        case .Fruits:
            return UIImage(named: "banana") ?? UIImage()
        }
    }
    
    var title: String {
        switch self{
        case .Drinks:
            return "Drinks"
        case .Desserts:
            return "Desserts"
        case .Food:
            return "Food"
        case .Salads:
            return "Salads"
        case .Soups:
            return "Soups"
        case .Vegetables:
            return "Vegetables"
        case .Fruits:
            return "Fruits"
        case .Pizza:
            return "Pizza"
        
        }
    }
}

class NoContactCell: UICollectionViewCell {
    
    let logoImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "safety")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Safe No-Contact Delivery"
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    let textField: UITextView = {
        let text = UITextView()
        text.isUserInteractionEnabled = false
        text.backgroundColor = .clear
        text.translatesAutoresizingMaskIntoConstraints = false
        text.text = "Keep yourself safe with social distancing"
        text.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        
        return text
    }()
    
    let learnMore: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Learn More"
        label.textColor = UIColor(red: 189/255, green: 98/255, blue: 59/255, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(logoImage)
        self.addSubview(label)
        self.addSubview(textField)
        self.addSubview(learnMore)
        
        logoImage.widthAnchor.constraint(equalToConstant: 65).isActive = true
        logoImage.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10).isActive = true
        logoImage.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        logoImage.heightAnchor.constraint(equalToConstant: 85).isActive = true
        
        label.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 11).isActive = true
        label.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        
        textField.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 5).isActive = true
        textField.leftAnchor.constraint(equalTo: label.leftAnchor,constant: -1).isActive = true
        textField.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        textField.rightAnchor.constraint(equalTo: self.centerXAnchor, constant: 20).isActive = true
        
        learnMore.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 11).isActive = true
        learnMore.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        
        self.backgroundColor = UIColor(red: 234/255, green: 207/255, blue: 139/255, alpha: 1)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class FoodItemCell: UICollectionViewCell {
    
    let logoImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "burger")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let textField: UITextView = {
        let text = UITextView()
        text.isUserInteractionEnabled = false
        text.backgroundColor = .clear
        text.translatesAutoresizingMaskIntoConstraints = false
        text.text = "Juicy Hamburger Extra Large"
        text.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        
        return text
    }()
    
    let subTextField: UITextView = {
        let text = UITextView()
        text.isUserInteractionEnabled = false
        text.backgroundColor = .clear
        text.translatesAutoresizingMaskIntoConstraints = false
        text.text = "Soda and Fries included!"
        text.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        text.textColor = UIColor(red: 193/255, green: 193/255, blue: 193/255, alpha: 1)
        
        return text
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "$8.99"
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    let templabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "HOT"
        label.textColor = UIColor(red: 189/255, green: 98/255, blue: 59/255, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(logoImage)
        self.addSubview(textField)
        self.addSubview(subTextField)
        self.addSubview(priceLabel)
        self.addSubview(templabel)
        
        logoImage.centerXAnchor.constraint(equalTo: self.centerXAnchor,constant: 0).isActive = true
        logoImage.widthAnchor.constraint(equalToConstant: 130).isActive = true
        logoImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        logoImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -130).isActive = true
        //logoImage.backgroundColor = .red
        
        textField.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 12).isActive = true
        textField.topAnchor.constraint(equalTo: logoImage.bottomAnchor, constant: 15).isActive = true
        textField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        textField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12).isActive = true
        
        subTextField.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 12).isActive = true
        subTextField.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 8).isActive = true
        subTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        subTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12).isActive = true
        
        priceLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 12).isActive = true
        priceLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
        
        templabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12).isActive = true
        templabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
        
        self.backgroundColor = UIColor.white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
